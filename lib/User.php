<?php

	include_once 'Session.php';
	include 'Database.php';

	/**
	* 
	*/
	class User {
		private $db;

		public function __construct() {
			$this -> db = new Database();
		}

		public function userRegistration($data) {
			$name 		= $data['name'];
			$username 	= $data['username'];
			$email		= $data['email'];
			$password	= $data['password'];

			$chk_email	= $this->emailCheck($email);

			if ($name == "" OR $username == "" OR $email == "" OR $password == "") {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Field must not be empty!</div>";
				return $msg;
			}

			if (strlen($username) < 3) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Username is too short!</div>";
				return $msg;
			} elseif (preg_match('/[^a-z0-9_-]+/i', $username)) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Username must only contain alphanumerical, dashes and underscore!</div>";
				return $msg;
			}

			if (strlen($password) < 3) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Password must be more than 3 digit long!</div>";
				return $msg;
			}

			$password	= md5($data['password']);

			if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Email address is not valid!</div>";
				return $msg;
			}

			if ($chk_email == true) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Email address already exists!</div>";
				return $msg;
			}

			$password = md5($data['password']);

			$sql = "INSERT INTO TBL_USER(NAME, USERNAME, EMAIL, PASSWORD) VALUES(:name, :username, :email, :password)";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':name', $name);
			$query->bindValue(':username', $username);
			$query->bindValue(':email', $email);
			$query->bindValue(':password', $password);
			$result = $query->execute();
			if ($result) {
				$msg = "<div class='alert alert-success'><strong>Success! </strong>Thank you, You have been registered.</div>";
				return $msg;
			} else {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Sorry! There has been problem inserting your details.</div>";
				return $msg;
			}

		}

		public function emailCheck($email) {
			$sql = "SELECT EMAIL FROM TBL_USER WHERE email = :email";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':email', $email);
			$query->execute();
			if ($query->rowCount() > 0) {
				return true;
			} else {
				return false;
			}
		}

		public function getLoginUser($email, $password) {
			$sql = "SELECT * FROM TBL_USER WHERE email = :email AND password = :password LIMIT 1";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':email', $email);
			$query->bindValue(':password', $password);
			$query->execute();
			$result = $query->fetch(PDO::FETCH_OBJ);
			return $result;
		}

		public function userLoginn($data) {
			$email		= $data['email'];
			$password	= md5($data['password']);

			$chk_email	= $this->emailCheck($email);

			if ($email == "" OR $password == "") {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Field must not be empty!</div>";
				return $msg;
			}

			if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Email address is not valid!</div>";
				return $msg;
			}

			if ($chk_email == false) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Email address not exists!</div>";
				return $msg;
			}

			$result = $this->getLoginUser($email, $password);

			if ($result) {
				Session::init();
				Session::set("login", true);
				Session::set("id", $result->id);
				Session::set("name", $result->name);
				Session::set("username", $result->username);
				Session::set("loginmsg", "<div class='alert alert-success'><strong>Success! </strong>You are loggedIn!</div>");
				header("Location: index.php");
			} else {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Data not found!</div>";
				return $msg;
			}

		}

		public function getUserData() {
			$sql = "SELECT * FROM TBL_USER ORDER BY ID DESC";
			$query = $this->db->pdo->prepare($sql);
			$query->execute();
			$result = $query->fetchAll();
			return $result;
		}

		public function getUserById($id) {
			$sql = "SELECT * FROM TBL_USER WHERE ID = :id LIMIT 1";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':id', $id);
			$query->execute();
			$result = $query->fetch(PDO::FETCH_OBJ);
			return $result;
		}

		public function updateUserData($id, $data) {
			$name 		= $data['name'];
			$username 	= $data['username'];
			$email		= $data['email'];

			if ($name == "" OR $username == "" OR $email == "") {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Field must not be empty!</div>";
				return $msg;
			}

			/* validation is being comment out
			if (strlen($username) < 3) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Username is too short!</div>";
				return $msg;
			} elseif (preg_match('/[^a-z0-9_-]+/i', $username)) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Username must only contain alphanumerical, dashes and underscore!</div>";
				return $msg;
			}

			if (strlen($password) < 3) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Password must be more than 3 digit long!</div>";
				return $msg;
			}

			$password	= md5($data['password']);

			if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Email address is not valid!</div>";
				return $msg;
			}

			if ($chk_email == true) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Email address already exists!</div>";
				return $msg;
			}
			*/

			$sql = "UPDATE TBL_USER SET 
						NAME 		= :name,
						USERNAME 	= :username,
						EMAIL 		= :email
					WHERE ID 		= :id";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':name', $name);
			$query->bindValue(':username', $username);
			$query->bindValue(':email', $email);
			$query->bindValue(':id', $id);
			$result = $query->execute();
			if ($result) {
				$msg = "<div class='alert alert-success'><strong>Success! </strong>Userdata updated Successfully!</div>";
				return $msg;
			} else {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Userdata Not Updated Yet!</div>";
				return $msg;
			}

		}

		private function checkPassword($id, $old_pass) {
			$password = md5($old_pass);
			$sql = "SELECT PASSWORD FROM TBL_USER WHERE ID = :id AND PASSWORD = :password";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':id', $id);
			$query->bindValue(':password', $password);
			$query->execute();
			if ($query->rowCount() > 0) {
				return true;
			} else {
				return false;
			}
		}

		public function updatePassword($id, $data) {
			$old_pass = $data['old_pass'];
			$new_pass = $data['password'];
			$chk_pass = $this->checkPassword($id, $old_pass);

			if ($old_pass == "" OR $new_pass == "") {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Field must not be empty!</div>";
				return $msg;
			}

			if ($chk_pass == false) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Old Password Not Exists!</div>";
				return $msg;
			}

			if (strlen($new_pass) < 4) {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Password is too short!!!</div>";
				return $msg;
			}

			$password = md5($new_pass);
			$sql = "UPDATE TBL_USER SET PASSWORD = :password WHERE ID = :id";
			$query = $this->db->pdo->prepare($sql);

			$query->bindValue(':password', $password);
			$query->bindValue(':id', $id);
			$result = $query->execute();
			if ($result) {
				$msg = "<div class='alert alert-success'><strong>Success! </strong>Password updated Successfully!</div>";
				return $msg;
			} else {
				$msg = "<div class='alert alert-danger'><strong>Error! </strong>Password Not Updated Yet!</div>";
				return $msg;
			}



		}




		
	}

?>